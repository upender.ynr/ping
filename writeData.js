var fs = require('fs');

function writeResultSetToFile(name, resultSet){

    var writeStream = fs.createWriteStream(name);
    json = JSON.stringify(resultSet);
    writeStream.write(json);
    writeStream.end();
}

module.exports = { writeResultSetToFile};