const write = require('./writeData');
const XLSX = require('xlsx');
const axios = require('axios');

process.env.UV_THREADPOOL_SIZE=64
const workbook = XLSX.readFile('test.xlsx');
const sheet_name_list = workbook.SheetNames;
const xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
//console.log(xlData)
const urls = xlData.map(function(elem){
    //.log('http://'+elem['Linked Domains']);
        return 'https://'+elem['Linked Domains'];
    });
     const dataDownUrl=[];

    // take3subtake1part1(urls);
    // async function take3subtake1part1(listOfArguments) {
    //     const concurrencyLimit = 1000;
    //     // Enhance arguments array to have an index of the argument at hand
    //     const argsCopy = [].concat(listOfArguments.map((val, ind) => ({ val, ind })));
    //     const result = new Array(listOfArguments.length);
    //     const promises = new Array(concurrencyLimit).fill(Promise.resolve());
    //     // Recursively chain the next Promise to the currently executed Promise
    //     function chainNext(p) {
    //       if (argsCopy.length) {
    //         const arg = argsCopy.shift();
    //         return p.then(() => {
    //           // Store the result into the array upon Promise completion
    //           const operationPromise =  axios.get(arg.val).catch(function(e){
    //                     console.log(arg.val,"down");
    //                      dataDownUrl.push(arg.val);
    //                  }).then(r => { result[arg.ind] = r; });
    //           return chainNext(operationPromise);
    //         });
    //       }
    //       return p;
    //     }
      
    //     await Promise.all(promises.map(chainNext));
    //     const writeResultSet =  write.writeResultSetToFile;
    //     writeResultSet('output.json', dataDownUrl);
    //     return result;
    //   }

let count=0;
let countTotal=0;

let promises = urls.map(url => {
    return axios.head(url).catch(function(e){
        
        dataDownUrl.push(url);
        count++;
        console.log(url,"Total Down: ",count);
    }).then(function(){
        countTotal++;
        console.log('Total Processed: ',countTotal);
    });
});

axios.all(promises).then(axios.spread(function (acct, perms) {
    const writeResultSet =  write.writeResultSetToFile;
    writeResultSet('output.json', dataDownUrl);
  })).catch(function(err){
      //console.log(err)
  });


